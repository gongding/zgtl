/*
 * @Description: Where 接口
 * @Author: ZPS
 */

package sqlx

// Where 接口实现 WhereMap or WhereSlice
type Where interface {
	Build() (whereSQL string, vals []any, err error)
}

// WhereMap map[string]any key为字段名与操作符:字段名 操作符 ，value为字段值
type WhereMap map[string]any

func (w WhereMap) Build() (whereSQL string, vals []any, err error) {
	return whereMapBuild(w)
}

// WhereSlice [][]any [“字段名”,“操作符”,“查询值”,“与前一个条件的关系[默认and]”]
type WhereSlice [][]any

func (w WhereSlice) Build() (whereSQL string, vals []any, err error) {
	return whereSliceBuild(w)
}
