/*
 * @Description: TODO
 * @Author: ZPS
 */

package sqlx

import (
	"fmt"
	"reflect"
	"testing"
)

func Test_whereMapBuild(t *testing.T) {
	type args struct {
		where map[string]any
	}
	tests := []struct {
		name         string
		args         args
		wantWhereSQL string
		wantVals     []any
		wantErr      bool
	}{
		{
			name: "案例1",
			args: args{
				where: map[string]any{"id in": []int{1, 2, 3}, "name": "zps", "age >": 18, "age <": 30, "utime between": Val{10000, 20000}},
			},
			wantWhereSQL: "`id` IN (?) AND `name` = ? AND `age` > ? AND `age` < ? AND `utime` BETWEEN ? AND ?",
			wantVals:     []any{[]int{1, 2, 3}, "zps", 18, 30, 10000, 20000},
			wantErr:      false,
		},
		{
			name: "案例2",
			args: args{
				where: map[string]any{"id <>": 1, "name like": "xx%"},
			},
			wantWhereSQL: "`id` != ? AND `name` LIKE ?",
			wantVals:     []any{1, "xx%"},
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cond, vals, err := whereMapBuild(tt.args.where)
			if (err != nil) != tt.wantErr {
				t.Errorf("whereBuild() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println("cond", cond)
			fmt.Println("wantWhereSQL", tt.wantWhereSQL)
			fmt.Println("vals", vals)
			fmt.Println("wantVals", tt.wantVals)
		})
	}
}

func Test_whereSliceBuild(t *testing.T) {
	type args struct {
		where [][]any
	}
	tests := []struct {
		name         string
		args         args
		wantWhereSQL string
		wantVals     []any
		wantErr      bool
	}{
		{
			name: "案例1",
			args: args{
				where: [][]any{
					{"id", "in", []int{1, 2, 3}},
					{"name = ? or name = ?", Exp, []any{"zps", "bon"}},
					{"sex", 1},
					{"age > ? AND age < ?", Exp, Val{18, 30}},
					{"utime", "between", Val{10000, 20000}},
				},
			},
			wantWhereSQL: "(id in ?) AND (name = ? or name = ?) AND (sex = ?) AND (age > ? AND age < ?) AND (utime BETWEEN ? AND ?)",
			wantVals:     []any{[]int{1, 2, 3}, "zps", "bon", 1, 18, 30, 10000, 20000},
			wantErr:      false,
		},
		{
			name: "案例2",
			args: args{
				where: [][]any{
					{"id", "=", 1},
					{"name", "like", "xx%"},
				},
			},
			wantWhereSQL: "(id = ?) AND (name like ?)",
			wantVals:     []any{1, "xx%"},
		},
	}
	//Raw
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cond, vals, err := whereSliceBuild(tt.args.where)
			fmt.Println("cond", cond)
			fmt.Println("wantWhereSQL", tt.wantWhereSQL)
			fmt.Println("vals", vals)
			fmt.Println("wantVals", tt.wantVals)
			if (err != nil) != tt.wantErr {
				t.Errorf("whereSliceBuild() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if cond != tt.wantWhereSQL {
				t.Errorf("whereSliceBuild() gotWhereSQL = %v, want %v", cond, tt.wantWhereSQL)
			}
			if !reflect.DeepEqual(vals, tt.wantVals) {
				t.Errorf("whereSliceBuild() gotVals = %v, want %v", vals, tt.wantVals)
			}
		})
	}
}

func TestWhereBuild(t *testing.T) {
	type args struct {
		where any
	}
	tests := []struct {
		name         string
		args         args
		wantWhereSQL string
		wantVals     []any
		wantErr      bool
	}{
		{
			name: "案例1",
			args: args{
				where: map[string]any{
					"id in":         []int{1, 2, 3},
					"name":          "zps",
					"age >":         18,
					"age <":         30,
					"utime between": Val{10000, 20000},
				},
			},
			wantWhereSQL: "`id` IN (?) AND `name` = ? AND `age` > ? AND `age` < ? AND `utime` BETWEEN ? AND ?",
			wantVals:     []any{[]int{1, 2, 3}, "zps", 18, 30, Val{10000, 20000}},
			wantErr:      false,
		},
		{
			name: "案例2",
			args: args{
				where: [][]any{
					{"id", "in", []int{1, 2, 3}},
					{"name = ? or name = ?", Exp, []any{"zps", "bon"}},
					{"sex", 1},
					{"age > ? AND age < ?", Exp, Val{18, 30}},
					{"utime", "between", Val{10000, 20000}},
				},
			},
			wantWhereSQL: "(id in ?) AND (name = ? or name = ?) AND (sex = ?) AND (age > ? AND age < ?) AND (utime BETWEEN ? AND ?)",
			wantVals:     []any{[]int{1, 2, 3}, "zps", "bon", 1, 18, 30, 10000, 20000},
			wantErr:      false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cond, vals, err := WhereBuild(tt.args.where)
			if (err != nil) != tt.wantErr {
				t.Errorf("whereBuild() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println("cond", cond)
			fmt.Println("wantWhereSQL", tt.wantWhereSQL)
			fmt.Println("vals", vals)
			fmt.Println("wantVals", tt.wantVals)
		})
	}
}
